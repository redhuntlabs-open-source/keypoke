module gitlab.com/redhuntlabs-open-source/keypoke

go 1.16

require (
	github.com/aws/aws-sdk-go v1.40.58
	github.com/weppos/publicsuffix-go v0.15.0
)
