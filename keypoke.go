package keypoke

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/weppos/publicsuffix-go/publicsuffix"
)

var (
	reSecrets          *regexp.Regexp
	dbFileName         = "regexes.json"
	secretTypes        = make([]string, 0)
	initBoundary       = "(?:^|[^0-9a-zA-Z])"
	exitBoundary       = "(?:$|[^0-9a-zA-Z])"
	reHosts            = regexp.MustCompile(`[A-Za-z0-9](?:[A-Za-z0-9.-]){2,63}\.[A-Za-z0-9]{2,18}`)
	regular            = regexp.MustCompile(`(?i)https?://[A-Za-z0-9](?:[A-Za-z0-9.-]){2,250}\.[A-Za-z0-9]{2,63}/[^\s\n'"]`)
	reMetaLines        = regexp.MustCompile(`Subject:[\s\S]+?diff --git[^\n]+\n|^(diff --git|[+-]{3})[^\n]+\n`)
	repoSrc            = regexp.MustCompile(`(?i)https?:\/\/((github\.com)|(bitbucket\.org)|(gitlab\.com))\/[A-Za-z0-9-_]*\/[^\/\[\]()#\.:\\\"'\n\* ]*`)
	mailSrc            = regexp.MustCompile(`(?i)\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b`)
	reSSH              = regexp.MustCompile(`SSH\-(\d{1}\.\d{1})\-.+?`)
	reJSSource         = regexp.MustCompile(`(?i)<script[^>]+src=['"]?([^'"\s>]+)`)
	reIP               = regexp.MustCompile(`^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$`)
	reMetaTags         = regexp.MustCompile(`(?i)<meta([^>]+)`)
	reMetaAttrs        = regexp.MustCompile(`(?i)([a-z0-9_-]+)=['"]?([^'">]+)`)
	re_domain          = regexp.MustCompile(`([a-zA-Z0-9-]+\.)+[a-zA-Z]+`)
	reSocial           = regexp.MustCompile(`(?i)https://(?:www\.)?(?:(?:instagram|facebook|twitter|github|gitlab|medium)\.com|reddit.com/u/|linkedin\.com/company)/[^/\\\'">,\n\s]+`)
	reBucket           = regexp.MustCompile(`(?i)https?://(?:[^\.]+?\.s3.*?\.amazonaws\.com|s3.*?\.amazonaws\.com/[^/'")(\s\n}{?&%><]+|.+?\.(?:nyc3\.|cdn\.)+digitaloceanspaces\.com|storage(?:\.cloud\.google\.com|\.googleapis\.com|-download\.googleapis\.com)/[^/'\")(\s\n}{?&%><]+|.+?\.(?:storage-download\.googleapis\.com|content-storage-(?:up|down)load\.googleapis\.com|blob\.core\.windows\.net|objects(?:-us-east-1)?\.cdn\.dream\.io))`)
	reDomain_https     = regexp.MustCompile(`https?://([a-zA-Z0-9-]+\.)+[a-zA-Z]+`)
	reTitle            = regexp.MustCompile(`(?i)<title[^>]*>([^<]+)</title>`)
	reEmail            = regexp.MustCompile(`(?i)[a-z0-9._-]+@[a-z0-9-\.]{2,253}\.[a-z]{2,}`)
	reYearRange        = regexp.MustCompile(`^\d{4}-\d{2}$`)
	reYear             = regexp.MustCompile(`\d{4}`)
	rehtmlComments     = regexp.MustCompile(`<!--[\s\S]*?-->`)
	re_copyright       = regexp.MustCompile(`(?i)(?:&copy;|©|>\s*Copyright(?:\s*(?:&#169;|&copy;|©)?)|&#169;)\s*([\d\s-]+)?(?:<a[^>]+?>)?([^<]+)(?:</a>)?`)
	ftpEndRegex        = regexp.MustCompile(`^(?:.*\r?\n)*([0-9]{3})( [^\r\n]*)?\r?\n$`)
	imapStatusEndRegex = regexp.MustCompile(`\r\n$`)
	pop3EndRegex       = regexp.MustCompile(`(?:\r\n\.\r\n$)|(?:\r\n$)`)
	smtpEndRegex       = regexp.MustCompile(`(?:^\d\d\d\s.*\r\n$)|(?:^\d\d\d-[\s\S]*\r\n\d\d\d\s.*\r\n$)`)
	reGAID             = regexp.MustCompile(`ga(['"]create['"], ['"]([^'"]+)['"], ['"]auto['"])`)
	reURI              = regexp.MustCompile(`"filename": "([^"]+)"`)
	wcObjectSrcRegex   = regexp.MustCompile(`(?m)object-src\s*'\*'`)
	httpSRCRegex       = regexp.MustCompile(`\-src\s*['\"]?http:\/\/`)
	maxageRegex        = regexp.MustCompile(`(?i)max-age\s*=\s*0`)
	reportURIRegex     = regexp.MustCompile(`report\s*=\s*http:\/\/\w+\.\w+\/`)
	modeBlockRegex     = regexp.MustCompile(`mode\s*=\s*block`)
	re_paste           = regexp.MustCompile(`"raw_url":"([^"]+)`)
	reCommit           = regexp.MustCompile(`"sha":"([a-f0-9]{40})"`)
	queryRegex         = regexp.MustCompile(`\b((xn--)?[a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}\b`)
	subRegex           = regexp.MustCompile(`(?i)(?:[a-z0-9-_\.]{1,255})[a-z0-9-]{2,63}\.[A-Za-z0-9]{2,18}`)
	dateRegex          = regexp.MustCompile(`\d{4}-\d{2}-\d{2}`)
	refavicon          = regexp.MustCompile(`(?i)<link[^>]+rel=['\"]?(?:shortcut |apple-touch-)?icon(?:-precomposed)?['\"]?[^>]+href=['\"]?([^'\"\s]+)`)
)

func filter(data []string, test func(string) bool) (result []string) {
	for _, item := range data {
		if test(item) {
			result = append(result, item)
		}
	}
	return
}

// Converts the slice elements to lowercase and removes duplicates
func rmDuplicates(slice *[]string) []string {
	keys := make(map[string]bool)
	list := []string{}

	for _, entry := range *slice {
		entry = strings.ToLower(entry)
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

// VerifyTLD - verifies whether a string is a valid domain or not
func VerifyTLD(mstring string) bool {
	_, err := publicsuffix.ParseFromListWithOptions(
		publicsuffix.DefaultList,
		mstring,
		&publicsuffix.FindOptions{
			IgnorePrivate: true,
		},
	)
	return err == nil
}

// ExtractHosts - extracts domains out of a string blob
func ExtractHosts(str string) []string {
	return filter(reHosts.FindAllString(str, -1), VerifyTLD)
}

// ExtractPatchfileHosts - Extracts hosts from patch files directly.
// Wraps a lot of other functions to give a complete list of hosts.
func ExtractPatchfileHosts(str *string) []string {
	var hosts []string
	mcontent := ParsePatchFile(str)
	vhosts := filter(reHosts.FindAllString(mcontent, -1), VerifyTLD)
	hosts = append(hosts, vhosts...)
	memail := ParseCommitEmail(str)
	for _, x := range memail {
		hosts = append(hosts, strings.Split(x, "@")[1])
	}
	return rmDuplicates(&hosts)
}

// ParseCommitEmail - Parses a patch file and returns the commit emails as a golang slice
func ParseCommitEmail(str *string) []string {
	var allemails []string
	var commitrex = regexp.MustCompile(`(?m)^From:\s[^<]*<([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)>`)
	matches := commitrex.FindAllStringSubmatch(*str, -1)
	for _, match := range matches {
		allemails = append(allemails, match[1])
	}
	return rmDuplicates(&allemails)
}

// ParsePatchFile - Parses a patch file and returns only stuff in the content
func ParsePatchFile(str *string) string {
	var totalparts []string
	var rex = regexp.MustCompile(`@@\s\-\d*(?:,\d*)?\s\+\d*(?:,\d*)?\s@@`)
	var reex = regexp.MustCompile(`From\s\w{40}`)
	diffs := rex.Split(*str, -1)[1:]
	for _, i := range diffs {
		if reex.MatchString(i) {
			totalparts = append(totalparts, reex.Split(i, -1)[0])
		} else if strings.Contains(i, "diff --git") {
			totalparts = append(totalparts, strings.Split(i, "diff --git a/")[0])
		} else {
			totalparts = append(totalparts, i)
		}
	}
	return strings.Join(totalparts, "\n")
}

// UpdateRegex - Updates regular expressions from out regex engine with a valid URL and key
func UpdateRegex() error {
	log.Println("Trying to download the db file...")
	bucket := "prod-regexes"

	file, err := os.Create(dbFileName)
	if err != nil {
		return err
	}

	defer file.Close()
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String("eu-west-2"),
		Credentials: credentials.NewEnvCredentials(),
	})
	if err != nil {
		return err
	}

	downloader := s3manager.NewDownloader(sess)
	_, err = downloader.Download(file,
		&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(dbFileName),
		},
	)
	if err != nil {
		return fmt.Errorf("unable to download item %q, %v", dbFileName, err)
	}

	log.Println("Successfully downloaded database file:", file.Name())

	body, err := ioutil.ReadFile(dbFileName)
	if err != nil {
		return err
	}

	var apiResponse = make(map[string]string)

	if err := json.Unmarshal(body, &apiResponse); err != nil {
		return err
	}

	secretTypes = []string{}
	secretRegexes := []string{}
	for typei, pattern := range apiResponse {
		secretTypes = append(secretTypes, typei)
		secretRegexes = append(secretRegexes, pattern)
	}
	reSecrets = regexp.MustCompile(initBoundary + "(?:(?:" + strings.Join(secretRegexes[:], ")|(?:") + "))" + exitBoundary)
	return nil
}

// CleanPatch - Cleans secret finder output
func CleanPatch(content string) string {
	return reMetaLines.ReplaceAllString(content, "")
}

// FindSecrets - Takes in a string and returns a slice of map containing secrets
func FindSecrets(content string) []map[string]string {
	allSecrets := []map[string]string{}
	matches := reSecrets.FindAllStringSubmatch(content, -1)
	for _, match := range matches {
		match := match[1:]
		for index, item := range match {
			if item != "" {
				thisSecret := map[string]string{}
				thisSecret["secret"] = item
				thisSecret["type"] = secretTypes[index]
				allSecrets = append(allSecrets, thisSecret)
			}
		}
	}
	return allSecrets
}

// GetSecrets - returns secret types and central regular expression
func GetSecrets() ([]string, *regexp.Regexp) {
	if err := UpdateRegex(); err != nil {
		panic(err)
	}
	if len(secretTypes) == 0 {
		panic(fmt.Errorf("no secrets types found, perhaps you missed UpdateRegex() ?"))
	}
	return secretTypes, reSecrets
}
