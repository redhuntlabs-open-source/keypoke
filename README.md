Requires two env vars:
- `AWS_ACCESS_KEY` - The regex AWS access key endpoint URL.
- `AWS_SECRET_KEY` - The regex AWS secret key used for auth.

Refer to Line 127 in [`keypoke.go`](keypoke.go#L127).
